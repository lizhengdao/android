package com.fungorn.tutorstudentchatapp.ui.auth.login

import android.app.ActivityOptions
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.fungorn.tutorstudentchatapp.R
import com.fungorn.tutorstudentchatapp.domain.UserType
import com.fungorn.tutorstudentchatapp.ui.MainActivity
import kotlinx.android.synthetic.main.fragment_login.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : Fragment(R.layout.fragment_login) {

    private val viewModel: LoginViewModel by viewModel()

    companion object {
        private const val ARG_USER_TYPE = "userType"

        fun bundleArgs(userType: UserType) = bundleOf(
            ARG_USER_TYPE to userType.name
        )
    }

    private val userType: UserType by lazy {
        UserType.valueOf(requireArguments().getString(ARG_USER_TYPE) ?: error("UserType required"))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observeFields()

        val errorIcon =
            ResourcesCompat.getDrawable(
                resources,
                R.drawable.ic_error_red_21dp,
                requireContext().theme
            )!!
        errorIcon.bounds = Rect(
            0,
            0,
            errorIcon.intrinsicWidth - 6,
            errorIcon.intrinsicHeight - 6
        )
/*
        loginEditText.addTextChangedListener(
            afterTextChanged = {
                if (loginEditText.text.isNullOrEmpty()) {
                    loginEditText.setError("", errorIcon)
                } else {
                    loginEditText.setError(null, null)
                }
            }
        )

        passwordEditText.addTextChangedListener(
            afterTextChanged = {
                if (passwordEditText.text.isNullOrEmpty()) {
                    passwordEditText.setError("", errorIcon)
                } else {
                    passwordEditText.setError(null, null)
                }
            }
        )

        loginButton.setOnClickListener {
            if (!loginEditText.text.isNullOrEmpty() && !passwordEditText.text.isNullOrEmpty()) {
                loginViewModel.login(
                    loginEditText.text.toString(),
                    passwordEditText.text.toString()
                )
            } else {
                loginEditText.setError("", errorIcon)
                passwordEditText.setError("", errorIcon)
                requireContext().showToast(resources.getString(R.string.error_login_is_empty))
            }
        }
 */

        button_login.setOnClickListener {
            viewModel.login(userType)
        }
    }

    private fun observeFields() = with(viewModel) {
        loginCompletedEvent.observe(viewLifecycleOwner) {
            navigateToMain()
        }
    }

    // TODO : Investigate if requireActivity().finish() is required or not
    private fun navigateToMain() = startActivity(
        Intent(activity, MainActivity::class.java),
        ActivityOptions.makeCustomAnimation(
            activity,
            R.anim.anim_slide_in_enter,
            R.anim.anim_slide_in_exit
        ).toBundle()
    ).also {
        requireActivity().finish()
    }
}