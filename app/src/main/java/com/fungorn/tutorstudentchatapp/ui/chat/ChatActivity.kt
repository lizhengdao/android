package com.fungorn.tutorstudentchatapp.ui.chat

import android.app.Activity
import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.fungorn.tutorstudentchatapp.R
import com.fungorn.tutorstudentchatapp.ui.chat.model.ChatType
import org.koin.androidx.viewmodel.ext.android.viewModel

class ChatActivity : AppCompatActivity() {

    private val viewModel: ChatViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        intent.extras?.let {
            val chatId = it.getInt(ARG_CHAT_ID)
            val chatType = it.getSerializable(ARG_CHAT_TYPE) as ChatType

            viewModel.onReceiveParams(chatId, chatType)
        } ?: throw IllegalStateException("Chat parameters must be provided")

    }

    companion object {

        private const val ARG_CHAT_ID = "chatId"
        private const val ARG_CHAT_TYPE = "chatType"

        fun openChat(fromActivity: Activity, chatId: Int, chatType: ChatType) =
            ContextCompat.startActivity(
                fromActivity,
                Intent(fromActivity, ChatActivity::class.java).apply {
                    putExtra(ARG_CHAT_ID, chatId)
                    putExtra(ARG_CHAT_TYPE, chatType)
                },
                ActivityOptions.makeCustomAnimation(
                    fromActivity,
                    R.anim.anim_slide_in_enter,
                    R.anim.anim_slide_in_exit
                ).toBundle()
            )
    }
}