package com.fungorn.tutorstudentchatapp.ui.direct

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.fungorn.tutorstudentchatapp.R
import com.fungorn.tutorstudentchatapp.ui.direct.model.DirectDialogModel
import kotlinx.android.synthetic.main.item_direct_dialog.view.*

class DirectDialogViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(item: DirectDialogModel, onDialogClick: (DirectDialogModel) -> Unit) = with(itemView) {
        textView_direct_name.text = item.senderName
        textView_direct_message.text = item.latestMessageText

        imageView_direct_avatar.load(/*TODO*/"") {
            placeholder(ColorDrawable(Color.GRAY))
            error(ContextCompat.getDrawable(context, R.drawable.ic_person_black_24dp))
        }
        imageView_direct_unreadIndicator.isVisible = item.hasUnreadMessages

        setOnClickListener { onDialogClick(item) }
    }
}