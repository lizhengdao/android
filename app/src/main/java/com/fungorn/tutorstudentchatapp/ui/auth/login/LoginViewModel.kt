package com.fungorn.tutorstudentchatapp.ui.auth.login

import androidx.lifecycle.ViewModel
import com.fungorn.tutorstudentchatapp.domain.UserType
import com.fungorn.tutorstudentchatapp.utils.SingleLiveEvent

class LoginViewModel : ViewModel() {
    val loginCompletedEvent: SingleLiveEvent<Unit> = SingleLiveEvent()

    fun login(userType: UserType) {
        loginCompletedEvent.call()
    }
}