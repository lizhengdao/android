package com.fungorn.tutorstudentchatapp.ui.profile

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.fungorn.tutorstudentchatapp.R
import com.fungorn.tutorstudentchatapp.ui.utils.DialogProvider
import kotlinx.android.synthetic.main.fragment_profile.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileFragment : Fragment(R.layout.fragment_profile) {

    private val viewModel: ProfileViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cardView_profile_avatarHolder

        textView_profile_changeStatus

        textView_profile_help.setOnClickListener {
            findNavController().navigate(R.id.fragment_help)
        }

        textView_profile_preferences.setOnClickListener {
            findNavController().navigate(R.id.fragment_settings)
        }

        textView_profile_logout.setOnClickListener {
            DialogProvider.confirmationDialog(
                requireContext(),
                getString(R.string.login_logout_confirmation),
                onConfirm = {
                    // TODO : Remove
                    requireActivity().finish()
                },
                onDecline = {}
            ).show()
        }
    }
}