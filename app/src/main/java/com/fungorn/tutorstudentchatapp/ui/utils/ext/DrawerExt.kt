package com.fungorn.tutorstudentchatapp.ui.utils.ext

import android.graphics.Color
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import com.fungorn.tutorstudentchatapp.R

fun DrawerLayout.setupWithContentScaling(
    activity: AppCompatActivity,
    content: View,
    contentScaleFactor: Float
) {
    val actionBarDrawerToggle: ActionBarDrawerToggle =
        object : ActionBarDrawerToggle(activity, this, R.string.open, R.string.close) {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                super.onDrawerSlide(drawerView, slideOffset)
                val slideX = drawerView.width * slideOffset
                content.translationX = slideX
                content.scaleX = 1 - (slideOffset / contentScaleFactor)
                content.scaleY = 1 - (slideOffset / contentScaleFactor)
            }
        }
    apply {
        setScrimColor(Color.TRANSPARENT)
        drawerElevation = 0f
        addDrawerListener(actionBarDrawerToggle)
    }
}