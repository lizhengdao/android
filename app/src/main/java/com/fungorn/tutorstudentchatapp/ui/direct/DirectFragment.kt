package com.fungorn.tutorstudentchatapp.ui.direct

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.fungorn.tutorstudentchatapp.R
import com.fungorn.tutorstudentchatapp.ui.chat.ChatActivity
import com.fungorn.tutorstudentchatapp.ui.chat.model.ChatType
import com.fungorn.tutorstudentchatapp.ui.utils.ext.doOnQuerySubmit
import com.fungorn.tutorstudentchatapp.utils.ext.observeNonNull
import kotlinx.android.synthetic.main.fragment_direct.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class DirectFragment : Fragment(R.layout.fragment_direct) {
    private val viewModel: DirectViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        searchView_direct_search.apply {
            findViewById<ImageView>(R.id.search_close_btn).setOnClickListener {
                setQuery("", false)
                clearFocus()
            }
            doOnQuerySubmit { query ->
                Toast.makeText(context, query, Toast.LENGTH_SHORT).show()
            }
        }

        val dialogAdapter = DirectDialogAdapter(onDialogClick = {
            ChatActivity.openChat(requireActivity(), it.id, ChatType.Direct)
        })
        recyclerView_direct_dialogs.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = dialogAdapter
        }

        viewModel.dialogs.observeNonNull(viewLifecycleOwner) { dialogs ->
            dialogAdapter.updateItems(dialogs)
        }
    }
}