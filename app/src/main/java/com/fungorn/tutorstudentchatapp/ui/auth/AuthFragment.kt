package com.fungorn.tutorstudentchatapp.ui.auth

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.fungorn.tutorstudentchatapp.R
import com.fungorn.tutorstudentchatapp.domain.UserType
import com.fungorn.tutorstudentchatapp.ui.auth.login.LoginFragment
import kotlinx.android.synthetic.main.fragment_auth.*

class AuthFragment : Fragment(R.layout.fragment_auth) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button_auth_loginAsTutor.setOnClickListener {
            findNavController().navigate(
                R.id.action_fragment_auth_to_fragment_login,
                LoginFragment.bundleArgs(UserType.TUTOR)
            )
        }
        button_auth_loginAsStudent.setOnClickListener {
            findNavController().navigate(
                R.id.action_fragment_auth_to_fragment_login,
                LoginFragment.bundleArgs(UserType.STUDENT)
            )
        }
    }
}