package com.fungorn.tutorstudentchatapp.ui.chat.info

import androidx.fragment.app.Fragment
import com.fungorn.tutorstudentchatapp.R

class ChatInfoFragment : Fragment(R.layout.fragment_chatinfo)