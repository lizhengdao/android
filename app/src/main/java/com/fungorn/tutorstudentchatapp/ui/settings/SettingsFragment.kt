package com.fungorn.tutorstudentchatapp.ui.settings

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.fungorn.tutorstudentchatapp.R
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingsFragment : Fragment(R.layout.fragment_settings) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        radio_option1.setOnCheckedChangeListener { _, isChecked ->
            Toast.makeText(requireContext(), "Radio #1: $isChecked", Toast.LENGTH_SHORT).show()
        }

        radio_option2.setOnCheckedChangeListener { _, isChecked ->
            Toast.makeText(requireContext(), "Radio #2: $isChecked", Toast.LENGTH_SHORT).show()
        }

        linearLayout_switch1.apply {
            setOnClickListener {
                switch1.toggle()
            }

            switch1.setOnCheckedChangeListener { _, isChecked ->
                Toast.makeText(requireContext(), "Switch #1: $isChecked", Toast.LENGTH_SHORT).show()
            }
        }
    }

}