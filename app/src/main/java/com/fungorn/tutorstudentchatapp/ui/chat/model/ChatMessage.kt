package com.fungorn.tutorstudentchatapp.ui.chat.model

import com.fungorn.tutorstudentchatapp.domain.MessageType
import com.fungorn.tutorstudentchatapp.domain.Poll
import com.stfalcon.chatkit.commons.models.IMessage
import com.stfalcon.chatkit.commons.models.IUser
import java.util.*

data class ChatMessage(
    private val id: String,
    private val text: String,
    private val user: ChatParticipant,
    val type: MessageType,
    val poll: Poll?,
    private val createdAt: Date
) : IMessage {
    override fun getId(): String = id
    override fun getCreatedAt(): Date = createdAt
    override fun getUser(): IUser = user
    override fun getText(): String = text
}