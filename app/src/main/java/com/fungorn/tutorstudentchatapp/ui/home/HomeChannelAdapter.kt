package com.fungorn.tutorstudentchatapp.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.fungorn.tutorstudentchatapp.R
import com.fungorn.tutorstudentchatapp.ui.home.model.HomeChannelModel

class HomeChannelAdapter(
    private val onItemClickListener: OnItemClickListener
) : RecyclerView.Adapter<HomeChannelViewHolder>() {
    private val differ = AsyncListDiffer(this, DIFF_CALLBACK)

    fun updateItems(items: List<HomeChannelModel>) {
        differ.submitList(items)
    }

    override fun getItemCount(): Int = differ.currentList.size

    override fun getItemViewType(position: Int): Int {
        return when (differ.currentList[position]) {
            is HomeChannelModel.HeaderItem -> R.layout.item_header
            is HomeChannelModel.ChannelItem -> R.layout.item_channel
            is HomeChannelModel.DirectItem -> R.layout.item_direct
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeChannelViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)
        val layoutRes = viewType
        return when (viewType) {
            R.layout.item_header -> HomeHeaderViewHolder(
                inflater.inflate(
                    layoutRes,
                    parent,
                    false
                )
            )
            R.layout.item_channel -> HomeChannelItemViewHolder(
                inflater.inflate(
                    layoutRes,
                    parent,
                    false
                )
            )
            R.layout.item_direct -> HomeDirectItemViewHolder(
                inflater.inflate(
                    layoutRes,
                    parent,
                    false
                )
            )
            else -> error("Unsupported view type: $viewType")
        }
    }

    override fun onBindViewHolder(holder: HomeChannelViewHolder, position: Int) {
        holder.bind(differ.currentList[position], onItemClickListener)
    }

    interface OnItemClickListener {
        fun onChannelClick(channel: HomeChannelModel.ChannelItem)
        fun onDirectClick(direct: HomeChannelModel.DirectItem)
    }

    companion object {
        // FIXME: 10.10.2020
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<HomeChannelModel>() {
            override fun areItemsTheSame(
                oldItem: HomeChannelModel,
                newItem: HomeChannelModel
            ): Boolean = oldItem == newItem

            override fun areContentsTheSame(
                oldItem: HomeChannelModel,
                newItem: HomeChannelModel
            ): Boolean = oldItem == newItem
        }
    }
}