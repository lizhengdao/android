package com.fungorn.tutorstudentchatapp.ui.chat

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fungorn.tutorstudentchatapp.data.api.ChatRepository
import com.fungorn.tutorstudentchatapp.ui.chat.model.ChatMessage
import com.fungorn.tutorstudentchatapp.ui.chat.model.ChatType
import com.fungorn.tutorstudentchatapp.utils.SingleLiveEvent
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class ChatViewModel(
    private val chatRepository: ChatRepository
) : ViewModel() {

    private val _title: MutableLiveData<String> = MutableLiveData()
    val title: LiveData<String> = _title

    val messageSentEvent: SingleLiveEvent<String> = SingleLiveEvent()

    init {
        chatRepository.observeText()
            .onEach { messageSentEvent.postValue(it) }
            .launchIn(viewModelScope)
    }

    fun onReceiveParams(chatId: Int, chatType: ChatType) {
        _title.value = when (chatType) {
            ChatType.Direct -> "Direct chat with USER"
            ChatType.Group -> "Group chat"
        }
    }

    fun sendMessage(message: ChatMessage) {
        viewModelScope.launch {
            chatRepository.sendText(message.text)
        }
    }
}