package com.fungorn.tutorstudentchatapp.ui.chat.model

enum class ChatType {
    Direct, Group
}