package com.fungorn.tutorstudentchatapp.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fungorn.tutorstudentchatapp.ui.home.model.HomeChannelModel

class HomeViewModel : ViewModel() {

    private val _channels = MutableLiveData<List<HomeChannelModel>>()
    val channels: LiveData<List<HomeChannelModel>> = _channels

    init {
        _channels.value = listOf(
            HomeChannelModel.HeaderItem("Channels"),
            HomeChannelModel.ChannelItem(1, "Channel #1", true),
            HomeChannelModel.ChannelItem(2, "Channel #2", false),
            HomeChannelModel.ChannelItem(3, "Channel #3", false),
            HomeChannelModel.HeaderItem("Directs"),
            HomeChannelModel.DirectItem(1, "Ivan Ivanov", true),
            HomeChannelModel.DirectItem(2, "Alexey Petrov", false)
        )
    }
}