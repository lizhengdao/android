package com.fungorn.tutorstudentchatapp.ui.chat

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import coil.api.load
import com.fungorn.tutorstudentchatapp.R
import com.fungorn.tutorstudentchatapp.domain.AvatarModel
import com.fungorn.tutorstudentchatapp.domain.MessageType
import com.fungorn.tutorstudentchatapp.domain.Poll
import com.fungorn.tutorstudentchatapp.ui.chat.model.ChatMessage
import com.fungorn.tutorstudentchatapp.ui.chat.model.ChatParticipant
import com.fungorn.tutorstudentchatapp.utils.ext.observeNonNull
import com.stfalcon.chatkit.messages.MessageHolders
import com.stfalcon.chatkit.messages.MessagesListAdapter
import com.stfalcon.chatkit.utils.DateFormatter
import kotlinx.android.synthetic.main.fragment_chat.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.text.SimpleDateFormat
import java.util.*

class ChatFragment : Fragment(R.layout.fragment_chat) {

    private val viewModel: ChatViewModel by sharedViewModel()

    private lateinit var messageAdapter: MessagesListAdapter<ChatMessage>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observeFields()

        messageAdapter = initAdapter()
        messagesList_chat.setAdapter(messageAdapter)

        messageInput_chat.setInputListener {
            Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
            // User message
            val message = buildDummyMessage(it.toString(), 0, "You")

            viewModel.sendMessage(message)

            messageAdapter.addToStart(message, true)

            true
        }
    }

    private fun observeFields() = with(viewModel) {
        title.observeNonNull(viewLifecycleOwner) { title ->
            textView_chat_title.text = title
        }

        messageSentEvent.observe(viewLifecycleOwner) {
            // Dummy duplicating message / TODO: WIP
            messageAdapter.addToStart(
//                buildDummyVoting(it.toString(), 1, "Partner"),
                buildDummyMessage(it.toString(), 1, "Partner"),
                true
            )
        }
    }

    private fun initAdapter(): MessagesListAdapter<ChatMessage> {
        val holdersConfig = MessageHolders().apply {
            setOutcomingTextConfig(
                ChatMessageViewHolder::class.java,
                R.layout.item_chat_outcoming_text,
                answerSelectListener
            )

            setIncomingTextConfig(
                ChatMessageViewHolder::class.java,
                R.layout.item_chat_incoming_text,
                answerSelectListener
            )
        }

        return MessagesListAdapter<ChatMessage>("0", holdersConfig) { imageView, url, _ ->
            imageView.load(url)
        }.apply {
            setDateHeadersFormatter(dateHeadersFormatter)
        }
    }

    private val dateHeadersFormatter by lazy {
        DateFormatter.Formatter {
            val today = Date()

            when {
                it.day == today.day && it.month == today.month && it.year == today.year -> getString(
                    R.string.chat_today
                )
                it.day + 1 == today.day && it.month == today.month && it.year == today.year -> getString(
                    R.string.chat_yesterday
                )
                else -> SimpleDateFormat("dd.MM", Locale.ENGLISH).format(it)
            }
        }
    }

    private val answerSelectListener by lazy {
        object : OnAnswerSelectListener {
            override fun onSelect(id: Int, text: String) {
                Toast.makeText(context, "Selected: $id : $text", Toast.LENGTH_SHORT).show()
            }
        }
    }

    // TODO : WIP
    private fun buildDummyMessage(text: String, senderId: Int, senderName: String) = ChatMessage(
        UUID.randomUUID().toString(),
        text,
        ChatParticipant(senderId, senderName, AvatarModel("", "#FFF")),
        MessageType.Ordinary,
        null,
        Date()
    )

    // TODO : WIP
    private fun buildDummyVoting(text: String, senderId: Int, senderName: String) = ChatMessage(
        UUID.randomUUID().toString(),
        text,
        ChatParticipant(senderId, senderName, AvatarModel("", "#FFF")),
        MessageType.Poll,
        Poll(
            listOf(
                Poll.AnswerOption(0, "answer #1"),
                Poll.AnswerOption(1, "answer #2"),
                Poll.AnswerOption(2, "answer #3")
            )
        ),
        Date()
    )

    interface OnAnswerSelectListener {
        fun onSelect(id: Int, text: String)
    }
}