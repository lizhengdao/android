package com.fungorn.tutorstudentchatapp.ui.utils.ext

import androidx.appcompat.widget.SearchView

fun SearchView.doOnQuerySubmit(throttleWindow: Long = 300L, onSubmit: (query: String) -> Unit) {
    setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        private var latestQueryChangeMillis: Long = System.currentTimeMillis()

        override fun onQueryTextSubmit(query: String?): Boolean {
            query?.let { onSubmit(it) }
            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            if (System.currentTimeMillis() - latestQueryChangeMillis > throttleWindow) {
                newText?.let { onSubmit(it) }
                latestQueryChangeMillis = System.currentTimeMillis()
            }
            return true
        }
    })
}