package com.fungorn.tutorstudentchatapp.data.api

import com.fungorn.tutorstudentchatapp.utils.Const
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.lang.reflect.Type

class DateTimeDeserializer : JsonDeserializer<DateTime> {
    override fun deserialize(
        json: JsonElement,
        typeOfT: Type,
        context: JsonDeserializationContext
    ): DateTime {
        return DateTimeFormat
            .forPattern(Const.DATE_FORMAT)
            .withOffsetParsed()
            .parseDateTime(json.asJsonPrimitive.asString)
    }
}