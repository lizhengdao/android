package com.fungorn.tutorstudentchatapp.data.api.websocket

import com.tinder.scarlet.ws.Receive
import com.tinder.scarlet.ws.Send
import kotlinx.coroutines.flow.Flow

interface WebsocketChatService {
    @Send
    fun sendText(text: String): Boolean

    @Receive
    fun observeText(): Flow<String>
}