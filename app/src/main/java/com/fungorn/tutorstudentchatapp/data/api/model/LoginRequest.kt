package com.fungorn.tutorstudentchatapp.data.api.model

data class LoginRequest(
    val username: String,
    val password: String
)