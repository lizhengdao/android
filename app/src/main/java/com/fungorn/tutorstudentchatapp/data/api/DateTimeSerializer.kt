package com.fungorn.tutorstudentchatapp.data.api

import com.fungorn.tutorstudentchatapp.utils.Const
import com.google.gson.JsonElement
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.lang.reflect.Type

class DateTimeSerializer : JsonSerializer<DateTime> {
    override fun serialize(
        src: DateTime,
        typeOfSrc: Type,
        context: JsonSerializationContext
    ): JsonElement {
        return JsonPrimitive(
            DateTimeFormat
                .forPattern(Const.DATE_FORMAT)
                .withOffsetParsed()
                .print(src)
        )
    }
}