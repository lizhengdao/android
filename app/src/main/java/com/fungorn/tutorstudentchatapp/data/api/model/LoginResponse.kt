package com.fungorn.tutorstudentchatapp.data.api.model

import org.joda.time.DateTime

data class LoginResponse(
    val authenticationToken: String,
    val expiresAt: DateTime,
    val refreshToken: String
)