package com.fungorn.tutorstudentchatapp.domain

data class Poll(
    val options: List<AnswerOption>
) {
    data class AnswerOption(
        val id: Int,
        val text: String
    )
}