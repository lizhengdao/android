package com.fungorn.tutorstudentchatapp.domain

data class Chat(
    val id: Int,
    val name: String,
    val participantIds: List<Int>
)