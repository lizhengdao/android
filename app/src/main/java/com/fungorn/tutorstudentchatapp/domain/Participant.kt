package com.fungorn.tutorstudentchatapp.domain

data class Participant(
    val id: Int,
    val name: String
)