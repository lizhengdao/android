package com.fungorn.tutorstudentchatapp.domain

import java.util.*

data class Message(
    val id: Int,
    val text: String,
    val senderId: Int,
    val chatId: Int,
    val createdAt: Date,
    val type: MessageType,
    val poll: Poll?
)