package com.fungorn.tutorstudentchatapp.domain

import org.joda.time.DateTime

sealed class User(
    val id: Int,
    val firstName: String,
    val middleName: String,
    val lastName: String,
    val type: UserType,
    val lastOnlineDate: DateTime
) {
    class Tutor(
        id: Int,
        firstName: String,
        lastName: String,
        middleName: String,
        lastOnlineDate: DateTime,
        val status: String
    ) : User(id, firstName, middleName, lastName, UserType.TUTOR, lastOnlineDate)

    class Student(
        id: Int,
        firstName: String,
        lastName: String,
        middleName: String,
        lastOnlineDate: DateTime,
        val groupName: String
    ) : User(id, firstName, middleName, lastName, UserType.STUDENT, lastOnlineDate)
}