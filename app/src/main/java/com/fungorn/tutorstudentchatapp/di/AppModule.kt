package com.fungorn.tutorstudentchatapp.di

import android.accounts.AccountManager
import android.content.Context
import com.fungorn.tutorstudentchatapp.data.AccountRepository
import com.fungorn.tutorstudentchatapp.data.api.ChatRepository
import com.fungorn.tutorstudentchatapp.ui.auth.login.LoginViewModel
import com.fungorn.tutorstudentchatapp.ui.chat.ChatViewModel
import com.fungorn.tutorstudentchatapp.ui.direct.DirectViewModel
import com.fungorn.tutorstudentchatapp.ui.home.HomeViewModel
import com.fungorn.tutorstudentchatapp.ui.profile.ProfileViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    single {
        AccountRepository(
            androidApplication().getSystemService(Context.ACCOUNT_SERVICE) as AccountManager
        )
    }
    single { ChatRepository(get()) }

    viewModel { LoginViewModel() }
    viewModel { HomeViewModel() }
    viewModel { DirectViewModel() }
    viewModel { ProfileViewModel() }
    viewModel { ChatViewModel(get()) }
}