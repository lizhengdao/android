package com.fungorn.tutorstudentchatapp.utils

object Const {
    const val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZZ"
}