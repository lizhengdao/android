package com.fungorn.tutorstudentchatapp

import android.app.Application
import com.fungorn.tutorstudentchatapp.di.appModule
import com.fungorn.tutorstudentchatapp.di.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class TutorStudentChatApp : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@TutorStudentChatApp)
            modules(appModule + networkModule)
        }
    }
}